import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { shallow } from 'enzyme'
import Digits from '../../App/Components/Digits'
import Stopwatch from '../../App/Components/Stopwatch'

describe('Sobre o componente Stopwatch', () => {

  let wrapper
  beforeEach(() => {
    wrapper = shallow(<Stopwatch />)
  })

  it('Deve renderizar dois componentes Digits', () => {
    let digitsArray = wrapper.find(Digits)
    expect(digitsArray).toHaveLength(2)
  })

  //Case esteje usando outro componente para o botão, é só importar e trocar os nomes
  it('Deve renderizar dois componentes TouchableOpacity', () => {
    let digitsArray = wrapper.find(TouchableOpacity)
    expect(digitsArray).toHaveLength(2)
  })
})

describe('Sobre o funcionamento dos botões', () => {

  //O botão reset deve ter um prop 'key' igual a 'resetButton'
  //O botão start/stop deve ter um prop 'key' igual a 'startStopButton'
  let wrapper, resetButton, startStopButton
  beforeEach(() => {
    wrapper = shallow(<Stopwatch />)
    resetButton = wrapper.findWhere((elem) => elem.key() === 'resetButton')
    startStopButton = wrapper.findWhere((elem) => elem.key() === 'startStopButton')
  })

  getUpdatedButton = (b) => {
    if (b === 'reset') resetButton = wrapper.findWhere((e) => e.key() === 'resetButton')
    else startStopButton = wrapper.findWhere((e) => e.key() === 'startStopButton')
  }

  it('O botão "reset" deve estar inicialmente deshabilitado', () => {
    expect(resetButton.props().disabled).toEqual(true)
  })

  //Deve obrigatóriamente declarar um prop 'disabled'
  it('O botão "start/stop" deve estar inicialmente habilitado', () => {
    expect(startStopButton.props().disabled).toEqual(false)
  })

  it('Clicar no botão "start/stop" deve habilitar o botão "reset"', () => {
    startStopButton.simulate('press')
    getUpdatedButton('reset')
    expect(resetButton.props().disabled).toEqual(false)
  })

  it('Clicar no botão "start/stop" não deve deshabilitar o próprio botão "start/stop"', () => {
    startStopButton.simulate('press')
    getUpdatedButton('startStop')
    expect(startStopButton.props().disabled).toEqual(false)
  })

  //Se o handler não existir, não gera erra nenhum
  //Totally possible to simulate clicks on disabled buttons
  it('Clicar no botão "reset" deve habilitar o botão "start/stop" e deshabilitar o "reset"', () => {
    startStopButton.simulate('press')
    resetButton.simulate('press')

    getUpdatedButton('reset')
    getUpdatedButton('startStop')
    expect(resetButton.props().disabled).toEqual(true)
    expect(startStopButton.props().disabled).toEqual(false)
  })
})

describe('Sobre a contagem', () => {
  let wrapper, inst, increase, startStopButton, resetButton
  beforeEach(() => {
    wrapper = shallow(<Stopwatch />)
    jest.useFakeTimers()

    //Spying on the prototype's method creates problems when using 'this'
    //Work on the INSTANCE's methods instead
    inst = wrapper.instance()
    inst.increase = jest.fn() //Mock the function you want to spy on
    increase = inst.increase

    startStopButton = wrapper.findWhere((elem) => elem.key() === 'startStopButton')
    resetButton = wrapper.findWhere((elem) => elem.key() === 'resetButton')
  })

  it('Clicar em "start/stop" pela primeira vez iniciará a contagem', () => {
    startStopButton.simulate('press')

    expect(increase).not.toHaveBeenCalled()
    jest.advanceTimersByTime(5000)
    expect(increase).toHaveBeenCalledTimes(5)
  })

  it('Clicar em "start/stop" uma segunda vez interromperá a contagem', () => {
    for (i = 0; i < 2; i++) {
      startStopButton.simulate('press')
      jest.advanceTimersByTime(5000)
    }
    expect(increase).toHaveBeenCalledTimes(5) //NOT 10
  })

  it('Clicar em "reset" interromperá a contagem', () => {
    startStopButton.simulate('press')
    jest.advanceTimersByTime(5000)
    resetButton.simulate('press')
    jest.advanceTimersByTime(5000)

    expect(increase).toHaveBeenCalledTimes(5)
  })
})

describe('Sobre a exibição dos dígitos', () => {

  let wrapper, inst, increase, startStopButton, resetButton, minDigits, secDigits
  beforeEach(() => {
    wrapper = shallow(<Stopwatch />)
    startStopButton = wrapper.findWhere((elem) => elem.key() === 'startStopButton')
    jest.useFakeTimers()
  })

  updateUI = () => {
    wrapper.update()
    secDigits = wrapper.findWhere((elem) => elem.key() === 'secDigits')
    minDigits = wrapper.findWhere((elem) => elem.key() === 'minDigits')
  }

  it('5 segundos após clicar em "start/stop", o valor exibido deve ser 00:05', () => {
    startStopButton.simulate('press')
    jest.advanceTimersByTime(5000)

    updateUI()
    expect(minDigits.props().value).toEqual('0')
    expect(secDigits.props().value).toEqual('5')
  })

  it('60 segundos após clicar em "start/stop", o valor exibido deve ser 01:00', () => {
    startStopButton.simulate('press')
    jest.advanceTimersByTime(60000)

    updateUI()
    expect(minDigits.props().value).toEqual('1')
    expect(secDigits.props().value).toEqual('0')
  })

  it('70 segundos após clicar em "start/stop", o valor exibido deve ser 01:10', () => {
    startStopButton.simulate('press')
    jest.advanceTimersByTime(70000)

    updateUI()
    expect(minDigits.props().value).toEqual('1')
    expect(secDigits.props().value).toEqual('10')
  })

  it('3600 segundos após clicar em "start/stop", o valor exibido deve ser 60:00', () => {
    startStopButton.simulate('press')
    jest.advanceTimersByTime(3600000)

    updateUI()
    expect(minDigits.props().value).toEqual('60')
    expect(secDigits.props().value).toEqual('0')
  })

})
