import React from 'react'
import { Text } from 'react-native'
import { shallow } from 'enzyme'
import Digits from '../../App/Components/Digits'

//DIGITS:
//Será uma dupla de dígitos XX que receberá o número que ele deve mostrar como prop

describe('Sobre o componente Digits', () => {

  let wrapper
  beforeEach(() => {
    wrapper = shallow(<Digits />) //Smoke test: renderiza sem erros
  })

  it('Renderiza exatamente um componente <Text>', () => {
    expect(wrapper.find(Text)).toHaveLength(1)
  })

  it('Renderiza exatamente dois dígitos dentro do <Text>', () => {
    let renderedText = wrapper.find(Text)
    expect(renderedText.props().children).toHaveLength(2)
    //console.log(renderedText.props())
  })
})

describe('Sobre os props', () => {

  it('Aceita um prop de nome "value" e renderiza-o dentro do <Text>', () => {
    let wrapper = shallow(<Digits value='XX'/>)
    let renderedText = wrapper.find(Text)
    expect(renderedText.props().children).toBe('XX')
  })

  it('Caso não seja passado um prop "value", renderiza "00"', () => {
    let wrapper = shallow(<Digits />)
    let renderedText = wrapper.find(Text)
    expect(renderedText.props().children).toBe('00')
  })

  it('Caso seja passado ao prop "value" uma string de tam < 2, adicione padding à direita e renderiza', () => {
    let wrapper = shallow(<Digits value='2'/>)
    let renderedText = wrapper.find(Text)
    expect(renderedText.props().children).toBe('02')
  })
})

describe('Sobre a função de validação', () => {

  //Primeiramente, ela deve existir, senão vai ser lançado um erro logo nessa linha
  let validateValue = jest.spyOn(Digits.prototype, 'validateValue')

  beforeEach(() => {
    validateValue.mockClear()
  })

  it('Deve ser chamada em dois momentos: mounting e updating', () => {
    let wrapper = shallow(<Digits value='20'/>)   //mounting
    wrapper.setProps({value: '21'})                //updating

    expect(validateValue).toHaveBeenCalledTimes(2)
    expect(validateValue).toHaveBeenNthCalledWith(1, {value: '20'})
    expect(validateValue).toHaveBeenNthCalledWith(2, {value: '21'})
  })

  //O propósito de Digits é só exibição
  //Lógica do cronômetro e recuperação de erros deve ser feito pelo componente Stopwatch

  it('Caso seja passado ao prop "value" algo que não é uma string, lançe um TypeError', () => {
    wrapperFn = () => { validateValue({value: {test: 'This is an object!'}}) }
    expect(wrapperFn).toThrow(TypeError)
  })

  it('Caso seja passado ao prop "value" uma string de tamanho > 2, lançe um RangeError', () => {
    wrapperFn = () => { validateValue({value: '22222'}) }
    expect(wrapperFn).toThrow(RangeError)
  })
})
