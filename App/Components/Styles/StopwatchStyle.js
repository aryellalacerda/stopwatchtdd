import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    //backgroundColor: 'green',
    //justifyContent: 'space-between',
    //alignItems: 'center',
  },
  clock: {
    flexDirection: 'row',
    flex: 1,
    //justifyContent: 'space-around',
    //backgroundColor: 'yellow',
    justifyContent: 'center',
    marginTop: 80,
    marginBottom: 80,
  },
  buttons: {
    flex: 1,
    //backgroundColor: 'orange',
    flexDirection: 'row',
    justifyContent: 'space-around',
    //alignItems: 'stretch',
  },
  button: {
    padding: 10,
    borderWidth: 1,
    //borderRadius: 3,
    //backgroundColor: 'silver',
  },
  buttonText: {
    fontSize: 25,
  },
  seperator: {
    fontSize: 70,
  }
})
