import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { View, Text } from 'react-native'
import styles from './Styles/DigitsStyle'

//TODO: Remove try/catch blocks from Digits component and move up to Stopwatch component

export default class Digits extends Component {

  static propTypes = {
    value: PropTypes.string
  }

  constructor(props) {
    super(props)

    //It's not possible to test this function if it isn't declared and bound like this
    this.validateValue = this.validateValue.bind(this)

    /*
    Used this.digits because value prop might need modifying and props are immutable,
    but using state instead of this.digits would have triggered unnecessary rerendering.
    */
    this.digits = '00'
  }

  //Perfectly safe to access this.props within componentWillMount
  componentWillMount = () => {
    this.validateValue(this.props)
  }

  componentWillReceiveProps = (nextProps) => {
    if (this.props !== nextProps) {
      this.validateValue(nextProps)
    }
  }

  zeroPadLeft = (value) => {
    return String('00' + value).slice(-2)
  }

  validateValue(props) {
    if (typeof(props.value) !== 'string' && typeof(props.value) !== 'undefined') {
      throw new TypeError("Digit's value prop must be of type 'string'")
    }

    if (typeof(props.value) === 'string') {
      if (props.value.length < 2) {
        this.digits = this.zeroPadLeft(props.value)
      }
      else if (props.value.length > 2) {
        throw new RangeError("Digit's value prop must be a string of length <= 2")
      }
      else {
        this.digits = props.value
      }
    }
  }

  render () {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>{this.digits}</Text>
      </View>
    )
  }
}
