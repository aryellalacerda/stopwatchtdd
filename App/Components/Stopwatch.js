import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity } from 'react-native'
import styles from './Styles/StopwatchStyle'
import Digits from './Digits'

export default class Stopwatch extends Component {

  //TODO: Verify if it's possible to do state = reset
  state = {
    value: 0,
    init: true,
    paused: false,
  }

  reset = {
    value: 0,
    init: true,
    paused: false,
  }

  // constructor() {
  //   super()
  // }

  increase = () => { this.setState({value: this.state.value+1}) }

  startCounter = () => { this.interval = setInterval(this.increase, 1000) }

  clearCounter = () => { clearInterval(this.interval) }

  startStopClickHandle = () => {
    if (this.state.init) {
      this.setState({init: false})
      this.startCounter()
    }
    else if (this.state.paused) {
      this.setState({paused: false})
      this.startCounter()
    }
    else {
      this.setState({paused: true})
      this.clearCounter()
    }
  }

  resetClickHandle = () => {
    this.setState(this.reset)
    this.clearCounter()
  }

  convertValueToDigits = () => {
    let t = this.state.value
    m = Math.floor(t / 60)
    s = t - (m * 60)
    return {mins: m, secs: s}
  }

  render () {

    let {mins, secs} = this.convertValueToDigits()

    return (
      <View style={styles.container}>

        <View style={styles.clock}>
          <Digits
            key='minDigits'
            value={mins.toString()}
          />
          <Text style={styles.seperator}>:</Text>
          <Digits
            key='secDigits'
            value={secs.toString()}
          />
        </View>

        <View style={styles.buttons}>
          <TouchableOpacity
            style={styles.button}
            key='startStopButton'
            disabled={false}
            onPress={this.startStopClickHandle}>
            <Text style={styles.buttonText}>Start/Stop</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.button}
            disabled={this.state.init}
            key='resetButton'
            onPress={this.resetClickHandle}>
            <Text style={styles.buttonText}>Reset</Text>
          </TouchableOpacity>
        </View>

      </View>
    )
  }
}
