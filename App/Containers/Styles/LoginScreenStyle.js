import { StyleSheet } from 'react-native'
import { ApplicationStyles } from '../../Themes/'

export default StyleSheet.create({
  fullScreen: {
    flex: 1,
    backgroundColor: 'black',
  },
  kbav: {
    backgroundColor: 'red',
  }
})
